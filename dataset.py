import requests
# retrieve the information for a datasts in the catalogue 'EU institutions'
# the dataset has the ID 'g3ebxpyzj2lulbjqwkhg'
dataset = "g3ebxpyzj2lulbjqwkhg"
url = "https://data.europa.eu/api/hub/search/datasets/" + dataset

response = requests.request("GET", url)

print(response.json())