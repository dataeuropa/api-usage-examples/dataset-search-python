# Python examples for data.europa.eu Search API

## Prerequisites

1. Python
2. Python packages:
   1. requests

## Examples

The examples can be run with `python {filename}`, e.g. `python search.py`

### Search

To search for a term, the Endpoint `/search` has to be used with the parameter `q`.

[search.py](search.py):
```python
import requests
# search for the term 'Covid 19'
url = "https://data.europa.eu/api/hub/search/search"
querystring = {"q": "Covid 19"}

response = requests.request("GET", url, params=querystring)

print(response.json())
```

### Search in specific catalogue

To limit the search to a specific catalogue, the `facets` query parameter has to be used with the catalog facet. 
Additionally, to enable the facet search, the query parameter `filter` has to be set to `datasets`:

[searchWithFilter.py](searchWithFilter.py):
```python
import requests
# search for the term 'Covid 19' in the catalogue 'EU institutions', which has the id 'european-union-open-data-portal'
url = "https://data.europa.eu/api/hub/search/search"

querystring = {"q": "Covid 19",
               "facets": '{"catalog":["european-union-open-data-portal"]}',
               "filter": "dataset"}

response = requests.request("GET", url, params=querystring)

print(response.json())
```

### Retrieve a specific catalogue

To retrieve the information for a specific catalogue, the endpoint `/catalogues` has to be used with the catalogue ID in the path.

[catalogue.py](catalogue.py):
```python
import requests
# retrieve the information for the catalogue 'EU institutions', which has the ID 'european-union-open-data-portal'
catalogue = "european-union-open-data-portal"
url = "https://data.europa.eu/api/hub/search/catalogues/" + catalogue

response = requests.request("GET", url)

print(response.json())
```

### Retrieve a specific dataset

To retrieve the information for a specific dataset, the endpoint `/datasets` has to be used with the dataset ID in the path.

[dataset.py](dataset.py):
```python
import requests
# retrieve the information for a datasts in the catalogue 'EU institutions'
# the dataset has the ID 'g3ebxpyzj2lulbjqwkhg'
dataset = "g3ebxpyzj2lulbjqwkhg"
url = "https://data.europa.eu/api/hub/search/datasets/" + dataset

response = requests.request("GET", url)

print(response.json())
```
