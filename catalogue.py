import requests
# retrieve the information for the catalogue 'EU institutions', which has the ID 'european-union-open-data-portal'
catalogue = "european-union-open-data-portal"
url = "https://data.europa.eu/api/hub/search/catalogues/" + catalogue

response = requests.request("GET", url)

print(response.json())
