import requests
# search for the term 'Covid 19' in the catalogue 'EU institutions', which has the id 'european-union-open-data-portal'
url = "https://data.europa.eu/api/hub/search/search"

querystring = {"q": "Covid 19",
               "facets": '{"catalog":["european-union-open-data-portal"]}',
               "filter": "dataset"}

response = requests.request("GET", url, params=querystring)

print(response.json())
